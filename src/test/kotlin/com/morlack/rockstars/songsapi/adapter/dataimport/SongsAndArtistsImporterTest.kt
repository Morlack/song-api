package com.morlack.rockstars.songsapi.adapter.dataimport

import com.morlack.rockstars.songsapi.domain.artist.Artist
import com.morlack.rockstars.songsapi.domain.Song
import com.morlack.rockstars.songsapi.domain.StorageProvider
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.http.HttpStatus
import org.springframework.web.client.RestTemplate

@ExtendWith(MockKExtension::class)
internal class SongsAndArtistsImporterTest {
    @MockK
    private lateinit var restTemplate: RestTemplate

    @MockK(relaxed = true)
    private lateinit var storageProvider: StorageProvider

    @InjectMockKs
    private lateinit var songsAndArtistsImporter: SongsAndArtistsImporter

    @BeforeEach
    internal fun setUp() {
        every { restTemplate.getForEntity("https://www.teamrockstars.nl/sites/default/files/artists.json", Array<ArtistImportDto>::class.java) } returns mockk {
            every { statusCode } returns HttpStatus.OK
            every { body } returns arrayOf(
                    ArtistImportDto(1, "Band without Metal song"),
                    ArtistImportDto(2, "Band without song before 2016"),
                    ArtistImportDto(3, "Band with multiple Metal songs in 2012"),
                    ArtistImportDto(4, "Band with Metal song in 1994"),
            )
        }
        every { restTemplate.getForEntity("https://www.teamrockstars.nl/sites/default/files/songs.json", Array<SongsImportDto>::class.java) } returns mockk {
            every { statusCode } returns HttpStatus.OK
            every { body } returns arrayOf(
                    SongsImportDto(1, "Not metal, boo", 1994, "Band without Metal song", "notmetal", 140, 2000, "Baby rock", "39872932", "Album no metal"),
                    SongsImportDto(2, "Too new song", 2020, "Band without song before", "noSongBefore", 150, 3000, "Metal", "83098203", "Young Album"),
                    SongsImportDto(3, "Metal in 2012 - part one", 2012, "Band with multiple Metal songs in 2012", "2020rocks1", 202, 392783, "Metal", "308203", "Metal one"),
                    SongsImportDto(4, "Metal in 2012 - part two", 2012, "Band with multiple Metal songs in 2012", "2020rocks2", 202, 392783, "Metal", "308204", "Metal one"),
                    SongsImportDto(5, "Metal in 1994", 1994, "Band with Metal song in 1994", "1994rocksmore", 220, 492783, "Metal", "23127391", "Metal rocks"),
                    SongsImportDto(6, "Metal in 2021", 2021, "Band with multiple Metal songs in 2012", "2021rocks2", 140, 39272183, "Metal", "324424", "Metal two"),
            )
        }
    }

    @Test
    internal fun `skips import when database contains artists`() {
        every { storageProvider.getArtistCount() } returns 1

        songsAndArtistsImporter.on(mockk())

        verify(exactly = 0) { storageProvider.store(any<Artist>()) }
        verify(exactly = 0) { storageProvider.store(any<Song>()) }
    }

    @Test
    internal fun `Import only artists with songs before 2016`() {
        every { storageProvider.getArtistCount() } returns 0

        songsAndArtistsImporter.on(mockk())

        verify { storageProvider.store(Artist("Band with multiple Metal songs in 2012")) }
        verify { storageProvider.store(Artist("Band with Metal song in 1994")) }
        verify(exactly = 0) { storageProvider.store(Artist("Band without Metal song")) }
        verify(exactly = 0) { storageProvider.store(Artist("Band without song before 2016")) }
    }

    @Test
    internal fun `Imports only songs before 2016`() {
        every { storageProvider.getArtistCount() } returns 0

        songsAndArtistsImporter.on(mockk())

        verify { storageProvider.store(match<Song> { it.name == "Metal in 2012 - part one" }) }
        verify { storageProvider.store(match<Song> { it.name == "Metal in 2012 - part two" }) }
        verify(exactly = 0) { storageProvider.store(match<Song> { it.name == "Metal in 2021" }) }
    }
}

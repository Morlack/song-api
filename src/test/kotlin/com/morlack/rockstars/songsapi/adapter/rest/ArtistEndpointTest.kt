package com.morlack.rockstars.songsapi.adapter.rest

import com.morlack.rockstars.songsapi.domain.artist.Artist
import com.morlack.rockstars.songsapi.domain.artist.ArtistService
import io.mockk.Runs
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.just
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
internal class ArtistEndpointTest {
    @MockK
    private lateinit var artistService: ArtistService

    @InjectMockKs
    private lateinit var artistEndpoint: ArtistEndpoint

    @Test
    internal fun `can create artist and returns the filled artist`() {
        every { artistService.create(any()) } answers {
            val artist = it.invocation.args[0] as Artist
            artist.id = 500L
            artist
        }

        val result = artistEndpoint.create(Artist("Some awesome name"))
        assertThat(result.name).isEqualTo("Some awesome name")
        assertThat(result.id).isEqualTo(500L)
    }

    @Test
    internal fun `retrieves song by name when name filter is supplied`() {
        every { artistService.findByName("Some name") } returns Artist("Some name")

        val result = artistEndpoint.find("Some name")
        assertThat(result).hasSize(1)
        assertThat(result[0].name).isEqualTo("Some name")
    }

    @Test
    internal fun `retrieves all song when no filter supplied`() {
        every { artistService.findAll() } returns listOf(
                Artist("Some name"),
                Artist("Some name2"),
        )

        val result = artistEndpoint.find(null)
        assertThat(result).hasSize(2)
        assertThat(result).extracting("name").containsAll(listOf("Some name", "Some name2"))
    }

    @Test
    internal fun `can update artist`() {
        every { artistService.update(any(), any()) } returnsArgument 1

        artistEndpoint.update(5000L, Artist("A new name"))
        verify { artistService.update(5000L, Artist("A new name")) }
    }

    @Test
    internal fun `can delete artist`() {
        every { artistService.deleteById(any()) } just Runs

        artistEndpoint.delete(5000L)
        verify { artistService.deleteById(5000L) }
    }
}

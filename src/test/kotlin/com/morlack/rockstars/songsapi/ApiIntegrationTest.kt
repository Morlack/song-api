package com.morlack.rockstars.songsapi

import com.fasterxml.jackson.databind.ObjectMapper
import com.morlack.rockstars.songsapi.domain.artist.Artist
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext


@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = ["classpath:application-integrationtest.properties"])
class ApiIntegrationTest {
    private lateinit var mvc: MockMvc

    @Autowired
    private lateinit var context: WebApplicationContext

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    val adminUser = user("admin").roles("API_WRITE", "API_READ")
    val readUser = user("read").roles("API_READ")

    @BeforeEach
    internal fun setUp() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply<DefaultMockMvcBuilder>(springSecurity())
                .build();
    }

    @Test
    internal fun `can not create multiple artists with the same name`() {
        createArtist("Artist 1")
                .andExpect(status().is2xxSuccessful)
        createArtist("Artist 1")
                .andExpect(status().is5xxServerError)
    }

    @Test
    internal fun `can not create an artist with the read role`() {
        mvc.perform(post("/artist")
                .with(readUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(Artist("Some name"))))
                .andExpect(status().isForbidden)
    }

    @Test
    internal fun `can not delete an artist with the read role`() {
        mvc.perform(delete("/artist/1")
                .with(readUser))
                .andExpect(status().isForbidden)
    }

    @Test
    internal fun `can not update an artist with the read role`() {
        mvc.perform(put("/artist/1")
                .with(readUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(Artist("Some name"))))
                .andExpect(status().isForbidden)
    }


    @Test
    internal fun `can not create multiple artists with different name`() {
        createArtist("Artist 2")
                .andExpect(status().is2xxSuccessful)
        createArtist("Artist 3")
                .andExpect(status().is2xxSuccessful)
    }

    @Test
    internal fun `can delete artist`() {
        createArtist("Artist 4")
                .andExpect(status().is2xxSuccessful)

        val artist = getArtistByName("Artist 4")
        deleteArtist(artist!!.id)

        mvc.perform(get("/artist?name=Artist 4")
                .with(adminUser)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect {
            val artists = objectMapper.readValue(it.response.contentAsString, Array<Artist>::class.java)
            assertThat(artists).isEmpty()
        }
    }

    @Test
    internal fun `can update an artist`() {
        createArtist("Artist 5")
                .andExpect(status().is2xxSuccessful)

        val artist = getArtistByName("Artist 5")

        mvc.perform(put("/artist/${artist!!.id}")
                .with(adminUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(Artist("Artist 99")))
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().is2xxSuccessful)

        val artistByName = getArtistByName("Artist 99")
        assertThat(artistByName).isNotNull.withFailMessage { "The artist was not updated" }
        assertThat(artistByName!!.id).isEqualTo(artist.id).withFailMessage { "The artist was inserted, not updated" }

    }

    private fun getArtistByName(name: String): Artist? {
        val response = mvc.perform(get("/artist?name=$name")
                .with(readUser)
                .accept(MediaType.APPLICATION_JSON)
        ).andReturn().response

        return objectMapper.readValue(response.contentAsString, Array<Artist>::class.java).getOrNull(0)
    }

    private fun createArtist(name: String): ResultActions {
        return mvc.perform(post("/artist")
                .with(adminUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(Artist(name))))
    }

    private fun deleteArtist(id: Long): ResultActions {
        return mvc.perform(delete("/artist/$id")
                .with(adminUser))
                .andExpect(status().is2xxSuccessful)
    }
}

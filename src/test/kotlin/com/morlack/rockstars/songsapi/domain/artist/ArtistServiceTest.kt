package com.morlack.rockstars.songsapi.domain.artist

import com.morlack.rockstars.songsapi.domain.StorageProvider
import com.morlack.rockstars.songsapi.domain.artist.exception.ApiErrorException
import io.mockk.CapturingSlot
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
internal class ArtistServiceTest {
    @MockK(relaxed = true)
    private lateinit var storageProvider: StorageProvider

    @InjectMockKs
    private lateinit var artistService: ArtistService

    @Test
    internal fun `can not insert new artist when one by that name already exists`() {
        every { storageProvider.findArtistByName("Some name 1") } returns Artist("Some name 1")

        assertThrows(ApiErrorException::class.java) {
            artistService.create(Artist("Some name 1"))
        }
    }

    @Test
    internal fun `can insert new artist when one by that name does not exists`() {
        every { storageProvider.findArtistByName("Some name 1") } returns null

        artistService.create(Artist("Some name 1"))
        verify { storageProvider.findArtistByName("Some name 1") }
        verify { storageProvider.store(Artist("Some name 1")) }
    }

    @Test
    internal fun `can not update existing artist when one by that name already exists`() {
        every { storageProvider.findArtistByName("Some name 33") } returns Artist("Some name 33")

        assertThrows(ApiErrorException::class.java) {
            artistService.update(25, Artist("Some name 33"))
        }
    }

    @Test
    internal fun `can update new artist when one by that name does not exists`() {
        every { storageProvider.findArtistByName("Some name 33") } returns null
        val existing = Artist("Some name 25")
        existing.id = 25L
        every { storageProvider.findArtistById(25L) } returns existing

        artistService.update(25, Artist("Some name 33"))
        verify { storageProvider.findArtistByName("Some name 33") }
        val slot = CapturingSlot<Artist>()
        verify { storageProvider.store(capture(slot)) }
        assertThat(slot.captured.id).isEqualTo(25)
        assertThat(slot.captured.name).isEqualTo("Some name 33")
    }
}

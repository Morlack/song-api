package com.morlack.rockstars.songsapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SongsApiApplication

fun main(args: Array<String>) {
	runApplication<SongsApiApplication>(*args)
}

package com.morlack.rockstars.songsapi.domain

import com.morlack.rockstars.songsapi.domain.artist.Artist

/**
 * The application can run in multiple configuration depending on the chosen storage strategy.
 * You can find implementations of the StorageProvider in `com.morlack.rockstars.songsapi.adapter` packages.
 * This is a strategy pattern implemented in spring, but it's not chosen at runtime but during Spring initialisation.
 */
interface StorageProvider {
    fun store(artist: Artist): Artist
    fun store(song: Song): Song

    fun findAllArtists(): List<Artist>
    fun findArtistById(id: Long): Artist?
    fun findArtistByName(name: String): Artist?
    fun getArtistCount(): Long
    fun getSongCount(): Long
    fun delete(artist: Artist)
}

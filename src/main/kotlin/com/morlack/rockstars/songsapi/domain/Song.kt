package com.morlack.rockstars.songsapi.domain

import com.morlack.rockstars.songsapi.domain.artist.Artist

data class Song(
        var name: String,
        var artist: Artist,
        val year: Int,
        val shortName: String?,
        val bpm: Int,
        val duration: Int,
        val genre: String,
        val spotifyId: String?,
        val album: String?,
) {
    var id: Long = -1
}

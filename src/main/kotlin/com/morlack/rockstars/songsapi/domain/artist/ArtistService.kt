package com.morlack.rockstars.songsapi.domain.artist

import com.morlack.rockstars.songsapi.domain.StorageProvider
import com.morlack.rockstars.songsapi.domain.artist.exception.ApiErrorException
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service

@Service
class ArtistService(
        private val storageProvider: StorageProvider
) {
    @Secured("ROLE_API_WRITE")
    fun create(artist: Artist): Artist {
        validateUniqueBandName(artist)
        return storageProvider.store(artist)
    }

    @Secured("ROLE_API_WRITE")
    fun update(id: Long, artist: Artist): Artist {
        validateUniqueBandName(artist)
        val existingArtist = storageProvider.findArtistById(id)
                ?: throw ApiErrorException("No artist exists with id $id, can not update")
        existingArtist.name = artist.name
        return storageProvider.store(existingArtist)
    }

    fun findByName(name: String): Artist? {
        return storageProvider.findArtistByName(name)
    }

    fun findAll(): List<Artist> {
        return storageProvider.findAllArtists()
    }

    @Secured("ROLE_API_WRITE")
    fun deleteById(id: Long) {
        val existingArtist = storageProvider.findArtistById(id)
                ?: throw ApiErrorException("No artist exists with id $id, can not delete")
        storageProvider.delete(existingArtist)
    }

    private fun validateUniqueBandName(artist: Artist) {
        val existingArtistByName = storageProvider.findArtistByName(artist.name)
        if (existingArtistByName != null) {
            throw ApiErrorException("Band with name ${artist.name} already exists with id ${existingArtistByName.id}")
        }
    }
}

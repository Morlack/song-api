package com.morlack.rockstars.songsapi.domain.artist

data class Artist(
        var name: String
) {
    var id: Long = -1
}

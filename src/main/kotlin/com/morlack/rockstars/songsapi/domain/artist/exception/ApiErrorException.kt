package com.morlack.rockstars.songsapi.domain.artist.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
class ApiErrorException(message: String): Exception(message) {
}

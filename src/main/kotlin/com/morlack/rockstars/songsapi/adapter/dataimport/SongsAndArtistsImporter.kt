package com.morlack.rockstars.songsapi.adapter.dataimport

import com.morlack.rockstars.songsapi.domain.artist.Artist
import com.morlack.rockstars.songsapi.domain.StorageProvider
import org.slf4j.LoggerFactory
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class SongsAndArtistsImporter(
        private val restTemplate: RestTemplate,
        private val storageProvider: StorageProvider,
) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    @EventListener
    fun on(event: ApplicationReadyEvent) {
        if (storageProvider.getArtistCount() > 0) {
            return
        }

        val artists = retrieveImportListData("https://www.teamrockstars.nl/sites/default/files/artists.json", Array<ArtistImportDto>::class.java)
        val songs = retrieveImportListData("https://www.teamrockstars.nl/sites/default/files/songs.json", Array<SongsImportDto>::class.java)

        songs.groupBy { it.artist }
                .mapValues { songsList -> songsIsOldEnough(songsList) }
                .filter { (artistName, _) -> artists.any { artist -> artist.name == artistName } }
                .mapKeys { (artistName, _) -> artists.first { artist -> artist.name == artistName } }
                .filter { (_, songs) -> containsMetal(songs) }
                .forEach { (artist, songs) ->
                    logger.info("Importing artist {} with songs {}", artist.name, songs.joinToString(separator = ", ") { it.name })
                    val storedArtist = storageProvider.store(Artist(artist.name))

                    songs.forEach { song ->
                        storageProvider.store(song.toDomain(storedArtist))
                    }
                }

        logger.info("Finished initial Artist and Song import. Database now contains {} artists with {} songs!", storageProvider.getArtistCount(), storageProvider.getSongCount())
    }

    private fun songsIsOldEnough(songsList: Map.Entry<String, List<SongsImportDto>>) =
            songsList.value.filter { song -> song.year < 2016 }

    private fun containsMetal(songs: List<SongsImportDto>) =
            songs.any { it.genre.contains("Metal") }

    private fun <T> retrieveImportListData(url: String, responseClass: Class<T>): T {
        val response = restTemplate.getForEntity(url, responseClass)
        if (!response.statusCode.is2xxSuccessful || response.body == null) {
            throw IllegalStateException("Can not import $url of an error during request.")
        }
        return response.body!!
    }
}

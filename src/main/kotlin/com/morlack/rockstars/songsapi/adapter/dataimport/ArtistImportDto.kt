package com.morlack.rockstars.songsapi.adapter.dataimport

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy::class)
data class ArtistImportDto(
        val id: Long,
        val name: String
)


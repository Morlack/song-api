package com.morlack.rockstars.songsapi.adapter.rest

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.security.SecurityScheme
import io.swagger.v3.oas.models.tags.Tag
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OpenApiConfiguration {
    @Bean
    fun openApi(): OpenAPI {
        return OpenAPI()
                .info(Info()
                        .title("Songs API")
                        .version("0.1")
                        .contact(Contact()
                                .name("Mitchell Herrijgers")
                                .email("mitchellherrijgers@gmail.com"))
                )
                .components(Components()
                        .addSecuritySchemes("basicAuth", SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("basic")))
                .tags(listOf(
                        Tag().name("Metal")
                ))
    }
}

package com.morlack.rockstars.songsapi.adapter.storage.database

import com.morlack.rockstars.songsapi.domain.Song
import com.morlack.rockstars.songsapi.domain.StorageProvider
import com.morlack.rockstars.songsapi.domain.artist.Artist
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.jdbc.support.KeyHolder
import org.springframework.stereotype.Service
import java.sql.ResultSet
import java.sql.Statement


/**
 * Provides database activity for storing songs and artists.
 * Uses Spring's JdbcTemplate since it makes it easier and prevents common errors.
 */
@Service
@ConditionalOnProperty("songs-api.storage-strategy", havingValue = "database", matchIfMissing = false)
class DatabaseStorageProvider(
        private val jdbcTemplate: JdbcTemplate
) : StorageProvider {
    override fun store(artist: Artist): Artist {
        if(artist.id != -1L) {
            // Is an update
            jdbcTemplate.update("UPDATE artist SET name=? WHERE id=?", artist.name, artist.id)
            return artist
        }
        val keyHolder: KeyHolder = GeneratedKeyHolder()
        jdbcTemplate.update({ connection ->
            val ps = connection.prepareStatement("INSERT INTO artist (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS)
            ps.setString(1, artist.name)
            ps
        }, keyHolder);

        artist.id = (keyHolder.key as Number).toLong()
        return artist
    }

    override fun store(song: Song): Song {
        if(song.id != -1L) {
            // Is an update
            jdbcTemplate.update("UPDATE song SET name=? WHERE id=?", song.name, song.id)
            return song
        }
        val keyHolder: KeyHolder = GeneratedKeyHolder()
        jdbcTemplate.update({ connection ->
            val ps = connection.prepareStatement("INSERT INTO song (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS)
            ps.setString(1, song.name)
            ps
        }, keyHolder);

        song.id = (keyHolder.key as Number).toLong()
        return song
    }

    override fun findAllArtists(): List<Artist> {
        return jdbcTemplate.query("SELECT id, name FROM artist", ArtistRowMapper())
    }

    override fun findArtistById(id: Long): Artist? = try {
        jdbcTemplate.queryForObject("SELECT id, name FROM artist WHERE id=?", ArtistRowMapper(), id)
    } catch (exception: EmptyResultDataAccessException) {
        null
    }

    override fun findArtistByName(name: String): Artist? = try {
        jdbcTemplate.queryForObject("SELECT id, name FROM artist WHERE name=?", ArtistRowMapper(), name)
    } catch (exception: EmptyResultDataAccessException) {
        null
    }

    class ArtistRowMapper : RowMapper<Artist> {
        override fun mapRow(rs: ResultSet, rowNum: Int): Artist {
            val artist = Artist(rs.getString("name"))
            artist.id = rs.getLong("id")
            return artist
        }
    }

    override fun getArtistCount(): Long {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM artist") { resultSet, _ ->
            resultSet.getLong(1)
        }!!
    }

    override fun getSongCount(): Long {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM song") { resultSet, _ ->
            resultSet.getLong(1)
        }!!
    }

    override fun delete(artist: Artist) {
        jdbcTemplate.update("DELETE FROM artist WHERE id=?", artist.id)
    }
}

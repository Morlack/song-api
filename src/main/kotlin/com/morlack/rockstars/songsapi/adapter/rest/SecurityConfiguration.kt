package com.morlack.rockstars.songsapi.adapter.rest

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.factory.PasswordEncoderFactories
import org.springframework.security.provisioning.InMemoryUserDetailsManager

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
class SecurityConfiguration: WebSecurityConfigurerAdapter() {
    private val passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder()

    /**
     * Configures the user accounts for usage in the API. Currently creates an "api" account and an "admin" account
     * with read and write priveleges respectively.
     *
     * This should be replaced by a database/LDAP/Oauth implementation before going live.
     */
    @Bean
    override fun userDetailsService(): UserDetailsService {
        val readUser = User.withUsername("api")
                .passwordEncoder(passwordEncoder::encode)
                .password("unsecure-api-read-password")
                .roles("API_READ")
                .build()


        val adminUser = User.withUsername("admin")
                .passwordEncoder(passwordEncoder::encode)
                .password("unsecure-admin-write-password")
                .roles("API_WRITE", "API_READ")
                .build()

        val userManager = InMemoryUserDetailsManager()
        userManager.createUser(readUser)
        userManager.createUser(adminUser)
        return userManager
    }

    /**
     * Configures security for the API
     * - No CSRF
     * - Every endpoint should be authenticated
     * - Except OpenAPI, which is open
     * - Admin checks are done through method security, not done here
     * - No sessions
     * - Http basic auth
     */
    override fun configure(http: HttpSecurity) {
        http.csrf().disable() // Not needed for an API without front-end.
                .authorizeRequests()
                .antMatchers("/swagger-ui.html", "/swagger-ui/**", "/v3/api-docs/**").permitAll()
                .antMatchers("/**").hasAnyAuthority("ROLE_API_READ")
                .anyRequest().authenticated()
                .and()
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }
}

package com.morlack.rockstars.songsapi.adapter.dataimport

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import com.morlack.rockstars.songsapi.domain.artist.Artist
import com.morlack.rockstars.songsapi.domain.Song

@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy::class)
data class SongsImportDto(
        val id: Long,
        val name: String,
        val year: Int,
        val artist: String,
        val shortName: String?,
        val bpm: Int,
        val duration: Int,
        val genre: String,
        val spotifyId: String?,
        val album: String?,
) {
    fun toDomain(domainArtist: Artist) = Song(
            name,
            domainArtist,
            year,
            shortName,
            bpm,
            duration,
            genre,
            spotifyId,
            album
    )
}

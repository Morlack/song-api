package com.morlack.rockstars.songsapi.adapter.storage.memory

import com.morlack.rockstars.songsapi.domain.Song
import com.morlack.rockstars.songsapi.domain.StorageProvider
import com.morlack.rockstars.songsapi.domain.artist.Artist
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import java.util.concurrent.atomic.AtomicLong

@Component
@ConditionalOnProperty("songs-api.storage-strategy", havingValue = "in-memory", matchIfMissing = true)
class InMemoryStorageProvider : StorageProvider {
    private val artistIdSequence = AtomicLong()
    private val artistMap: MutableMap<Long, Artist> = mutableMapOf()

    private val songIdSequence = AtomicLong()
    private val songMap: MutableMap<Long, Song> = mutableMapOf()

    override fun store(artist: Artist): Artist {
        if (artist.id == -1L) {
            artist.id = artistIdSequence.incrementAndGet()
            artistMap[artist.id] = artist
            return artist
        }

        // Already has an id, was updated. Store in the map.
        artistMap[artist.id] = artist
        return artist
    }

    override fun store(song: Song): Song {
        if (song.id == -1L) {
            song.id = songIdSequence.incrementAndGet()
            songMap[song.id] = song
            return song
        }

        // Already has an id, was updated. Store in the map.
        songMap[song.id] = song
        return song
    }

    override fun findAllArtists(): List<Artist> {
        return artistMap.values.toList()
    }

    override fun findArtistById(id: Long): Artist? {
        return artistMap[id]
    }

    override fun findArtistByName(name: String): Artist? {
        return artistMap.values.firstOrNull { it.name == name }
    }

    override fun getArtistCount(): Long {
        return artistMap.size.toLong()
    }

    override fun getSongCount(): Long {
        return songMap.size.toLong()
    }

    override fun delete(artist: Artist) {
        artistMap.remove(artist.id)
    }
}

package com.morlack.rockstars.songsapi.adapter.rest

import com.morlack.rockstars.songsapi.domain.artist.Artist
import com.morlack.rockstars.songsapi.domain.artist.ArtistService
import io.swagger.v3.oas.annotations.Operation
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("artist")
class ArtistEndpoint(
        private val artistService: ArtistService
) {
    @PostMapping
    @Operation(summary = "Creates a new artist with the provided name. The name must be unique.")
    fun create(@RequestBody artist: Artist): Artist {
        validate(artist)
        return artistService.create(artist)
    }

    @PutMapping("/{id}")
    @Operation(summary = "Updates a new artist. The name must be unique.")
    fun update(@PathVariable id: Long, @RequestBody artist: Artist): Artist {
        validate(artist)
        return artistService.update(id, artist)
    }

    @GetMapping
    @Operation(summary = "Retrieves all artists, or finds an artist by name by providing a filter.")
    fun find(@RequestParam("name") name: String?): List<Artist> {
        if (name != null) {
            return listOfNotNull(artistService.findByName(name))
        }
        return artistService.findAll()
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Retrieves an artist, erasing it from Metal history.")
    fun delete(@PathVariable id: Long) {
        artistService.deleteById(id)
    }

    private fun validate(artist: Artist) {
        if (artist.id != -1L) {
            // This can be solved by using DTOs for the API. Might be a good next step to take
            throw IllegalArgumentException("You are forcing the ID of the Artist. This is not allowed!")
        }
    }
}

# Rockstars IT Assignment: CRUD Songs API

This project provides a Rocks songs API that can be queried:

- (C): Het kunnen toevoegen van nieuwe band(s) of nummers met alle details die ook in de JSON staan waarbij de naam van de band natuurlijk niet dubbel voor mag komen;
- (R): Het kunnen zoeken naar een band op naam of nummers op genre (voor wanneer je echt zin hebt in Finse Death Metal);
- (U): Het kunnen updaten van alle informatie gerelateerd aan bands en nummers  op basis van de bijbehorende id’s; 
- (D): Het kunnen verwijderen van een band of nummer op basis van de id


## Initial Data
Data is initially filled using these two provided JSON files:
- https://www.teamrockstars.nl/sites/default/files/artists.json
- https://www.teamrockstars.nl/sites/default/files/songs.json

The application will load these when the database is found to be empty on the first run. 

# Usage

There are multiple ways to use/test the application:

- You can access the [OpenAPI documentation](http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config)
- You can use the `examples.http` file provided in the project to run requests in Intellij (if you have the plugin enabled)

## Authentication
Endpoints are authenticated and only someone with an admin role can create, update or delete artists and songs. 

The authentication implementation is currently a fixed set of two users. This should be moved to a database, oauth or LDAP implementation soon.

| Username | Password | Role |
| --- | --- | --- |
| admin | unsecure-admin-write-password | API_WRITE |
| api | unsecure-api-read-password | API_READ |

The roles are managed with `@Secured` annotation on the service layer. These specify the required role. All endpoint are by default secured to need an `API_READ` role.

If you use the [OpenAPI documentation](http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config) you should use the login form first, on the top right, with these credentials.

# Running

There are two ways to run the application: Through deployment in a docker container, and through IntelliJ runconfigurations. Both use a docker-compose for the provided persistence and caching providers.

## IntelliJ
When opening this project in IntelliJ there will be run configurations automatically configured to run.
Whatever configuration you choose, you must run the docker-compose dev stack:

`docker-compose -f docker-compose-dev.yaml up -d`


## Docker-compose
You can also run the application in docker containers using docker-compose. The file has been prepared for immediate usage.

You can run the following commands to get started:
```shell
mvn clean install # Creates the JAR files
docker-compose build # Create the applicaton docker-container
docker-compose up -d # Starts the docker containers
```

Now you can access the application at [localhost:8080](http://localhost:8080)
